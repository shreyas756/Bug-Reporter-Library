import BugRepoter from "./lib/BugReporter";

console.defaultError = console.error.bind(console);
console.errors = [];
console.error = function () {
  console.defaultError.apply(console, arguments);
  console.errors.push(Array.from(arguments));
};

export default BugRepoter;
