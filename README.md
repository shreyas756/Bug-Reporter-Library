# [Bug Reporter Library]

bug-reporter-library is a library for reporting the bug and generating a bug report.

# Installation

**Note: Use [yarn](https://yarnpkg.com/en/docs/install) instead of [npm](https://www.npmjs.com/)**

For adding the library in your App, run following command :
` $yarn add bug-reporter-library`

#### Steps: to use this library in your project:

1. Add this library to your application.
2. Create a reference on your root components by using `createRef` hook

3. provide a mandate reference props use function to pass prop library - ` getTarget` and define this _getTarget_ method by following

```

 const getTarget = () => {

 return ref.current;

 };

```

4. Provide the buttonStyle prop to library to set the position of button or else it will take default style as -

`{ position: "fixed", bottom: 90, right: 100 }`

5. provide a API end-point as url prop for a post request call, to which it will submit a object with keys -

` {image: , description: , ipAddress: , email: , browserName: , path: }` and where `image` will hold image url of final marked image, `description` will hold bug description provided, `path` will hold path of the page url where bug is been reported, `email` will hold email address of the user who is reporting that bug, `ipAddress` will hold IP address and `browserName` will hold current browser name where application is been executed.

6. Provide a reference prop `onSubmitBug` which will provide you a bug report in form of a object.Define this _onSubmitBug_ method by following

```

 const onSubmitBug = (data) => {

 console.log(data);

 //can get all the useful information here

 };

```

## How to use this libraray-

- Include this in your script.

```
<BugReporter
        getTarget={getTarget}
        buttonStyle={{ position: "fixed", bottom: 90, right: 100 }}
        onSubmitBug={onSubmitBug}
        url={"http://localhost:8000/bugReports"}
/>

```

## How it works

After setting up all above in your project run `yarn start` to start your project.

- Then user will get a button in your application by clicking it will be take into a Modal where they can have marker to chose different color and different size.now using it they can mark multiple bugs and put a description on each.

- User will get reset button for reset your Modal screen and close button to close that model

- also user can get one description box bellow the Modal where can describe more information and submit button will do a POST http request and all the useful information will send onto that given url end-point and will store there into database.

### External Library it uses

1. `@ant-design/icons`

2. `@tippy.js/react`

3. `html2canvas`

4. `use-react-screenshot`
