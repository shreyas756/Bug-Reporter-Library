import { CloseSquareOutlined, RedoOutlined, SyncOutlined, UndoOutlined, ZoomInOutlined, ZoomOutOutlined } from "@ant-design/icons";
import Tippy from "@tippy.js/react";
import React, { useRef, useEffect, useState } from "react";
import whichBrowser from "browser-name";
import { Modal, Button, ButtonGroup } from "react-bootstrap"; // import logo from './logo.png';
// import './BugReporter.css';

import * as htmlToImage from 'html-to-image';
import { FaRegHandRock } from "react-icons/fa";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
export default function BugReporter(props) {
  const [modalState, setModalState] = useState({
    description: "",
    email: "",
    isDrawing: false,
    isEditing: false,
    text: null,
    textList: [],
    color: "#0000FF",
    lineWidth: 5,
    isFinal: false,
    isFirst: false
  });
  const {
    description,
    email,
    isDrawing,
    isEditing,
    text,
    textList,
    color,
    lineWidth,
    isFinal,
    isFirst
  } = modalState;
  const [dragZoom, setDragZoom] = useState(true);
  const [modalImage, setModelImage] = useState();
  const [image, setNewImage] = useState(null);
  const [newImage, setImage] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const canvasRef = useRef(null);
  const contextRef = useRef(null);
  const modalRef = useRef();

  const onChange = (e, item) => {
    setModalState({ ...modalState,
      [item]: e.currentTarget.value
    });
  };

  const resetCanvas = () => {
    setModalState({ ...modalState,
      textList: []
    });
    redrawCanvas();
  };

  const openModal = () => {
    htmlToImage.toPng(props.getTarget()).then(function (dataUrl) {
      setNewImage(dataUrl);
      setIsOpen(true);
      resetCanvas();
      setModalState({ ...modalState,
        isFirst: true
      });
    });
  };

  const closeModal = () => {
    setIsOpen(false);
    setDragZoom(true);
    setModalState({ ...modalState,
      description: "",
      textList: [],
      isFinal: false,
      isFirst: false
    });
  };

  const redrawCanvas = () => {
    // adjusting canvas dimensions to fit page
    const canvas = canvasRef.current;
    canvas.width = window.innerWidth * 1.4;
    canvas.height = window.innerHeight * 1.4;
    canvas.style.width = `${window.innerWidth}px`;
    canvas.style.height = `${window.innerHeight}px`; // adjusting image dimensions to fit canvas

    const imageWidth = canvas.width;
    const imageHeight = canvas.height; // adding image to the canvas

    const context = canvas.getContext("2d");
    const img = new Image();
    img.src = image;
    context.drawImage(img, 0, 0, imageWidth, imageHeight);
    context.scale(2, 2); //error correction for mouse diff

    context.lineCap = "round";
    contextRef.current = context; // adding zoom function to canvas 
  };

  useEffect(() => {
    if (isFinal) {
      reportBug();
    } else {
      console.log(contextRef.current);

      if (contextRef.current && canvasRef.current) {
        redrawCanvas();
      }
    } // eslint-disable-next-line

  }, [image, isFinal, isFirst]);

  const startDrawing = ({
    nativeEvent
  }) => {
    contextRef.current.strokeStyle = color;
    contextRef.current.lineWidth = lineWidth;
    const {
      offsetX,
      offsetY
    } = nativeEvent;
    contextRef.current.beginPath();
    contextRef.current.moveTo(offsetX, offsetY);
    setModalState({ ...modalState,
      isDrawing: true
    });
  };

  const endDrawing = () => {
    contextRef.current.closePath();
    setModalState({ ...modalState,
      isDrawing: false
    });
  };

  const draw = ({
    nativeEvent
  }) => {
    if (!isDrawing || !dragZoom) return;
    const {
      offsetX,
      offsetY
    } = nativeEvent;
    contextRef.current.lineTo(offsetX, offsetY);
    contextRef.current.stroke();
  };

  const showTextField = () => {
    setModalState({ ...modalState,
      text: "",
      isEditing: true
    });
  };

  const saveText = e => {
    setModalState({ ...modalState,
      text: e.currentTarget.value
    });
  };

  const saveTextList = e => {
    if (e.currentTarget.value !== "") {
      const list = [...textList, text];
      setModalState({ ...modalState,
        textList: list,
        isEditing: false
      });
    } else {
      setModalState({ ...modalState,
        isEditing: false
      });
    }
  };

  const dragEnd = e => {
    let x = window.innerWidth > 1440 ? 450 : 200;
    let y = window.innerHeight > 798 ? 175 : 135;
    e.currentTarget.setAttribute("style", `position: absolute; left: ${e.clientX - x}px; top: ${e.clientY - y}px`);
  };

  const hideTextField = e => {
    setModalState({ ...modalState,
      isEditing: false
    });
  };

  const addBugs = () => {
    htmlToImage.toPng(modalRef.current).then(function (dataUrl) {
      setModelImage(dataUrl);
      setModalState({ ...modalState,
        isFinal: true
      });
    });
  };

  const findOsName = () => {
    let OSName = "unknown";
    if (navigator.userAgent.indexOf("Win") !== -1) OSName = "windows";
    if (navigator.userAgent.indexOf("Mac") !== -1) OSName = "mac";
    if (navigator.userAgent.indexOf("Linux") !== -1) OSName = "linux";
    if (navigator.userAgent.indexOf("Android") !== -1) OSName = "android";
    if (navigator.userAgent.indexOf("like Mac") !== -1) OSName = "iOS";
    return OSName;
  };

  const convertURIToImageData = URI => {
    return new Promise(function (resolve, reject) {
      if (URI == null) return reject();
      var canvas = document.createElement('canvas'),
          context = canvas.getContext('2d'),
          image = new Image();
      image.addEventListener('load', function () {
        canvas.width = image.width;
        canvas.height = image.height;
        context.drawImage(image, 0, 0, canvas.width, canvas.height);
        resolve(context.getImageData(0, 0, canvas.width, canvas.height));
      }, false);
      image.src = URI;
    });
  };

  const setDrag = () => {
    setDragZoom(!dragZoom);
  };

  const reportBug = async () => {
    const browserName = whichBrowser();
    const data = await fetch("http://ip-api.com/json").then(response => response.json());

    if (!description.length) {
      alert("Please enter bug description");
    } else {
      convertURIToImageData(modalImage).then(function (imageData) {
        setImage(imageData);
      });
      let bodyForm = new FormData();
      const OS = findOsName();
      bodyForm.append('image', newImage);
      bodyForm.append('description', description);
      bodyForm.append('email', email);
      bodyForm.append('browserName', browserName);
      bodyForm.append('name', props.name);
      bodyForm.append('system', OS);
      bodyForm.append('ipAddress', data.query);
      bodyForm.append('path', window.location.href);
      bodyForm.append('isArchive', false);
      bodyForm.append('errors', console.errors);

      if (props.url) {
        fetch(props.url, {
          method: "POST",
          headers: {
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*"
          },
          body: bodyForm
        }).then(() => {
          closeModal();
          props.onSubmitBug(bodyForm);
        }).catch(err => {
          setModalState({ ...modalState,
            isFinal: false
          });
          closeModal();
        });
      } else {
        closeModal();
        props.onSubmitBug(bodyForm);
      }
    }
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Button, {
    style: props.buttonStyle,
    className: "button_bug",
    icon: "plus",
    variant: "primary",
    onClick: openModal,
    size: "lg"
  }, "Report bug"), /*#__PURE__*/React.createElement(Modal, {
    show: isOpen,
    onHide: closeModal
  }, /*#__PURE__*/React.createElement("div", {
    className: "modal-style bug_model"
  }, /*#__PURE__*/React.createElement(Modal.Header, {
    closeButton: true
  }, /*#__PURE__*/React.createElement("div", {
    className: ""
  }), /*#__PURE__*/React.createElement(Modal.Title, {
    className: "modalHeader"
  }, /*#__PURE__*/React.createElement("div", {
    className: "container-tools"
  }, /*#__PURE__*/React.createElement("div", {
    className: "tools_contain"
  }, /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Reset Now"
  }, /*#__PURE__*/React.createElement(UndoOutlined, {
    style: {
      color: "#660066"
    },
    onClick: resetCanvas
  })), isEditing ? /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Hide Text Box"
  }, /*#__PURE__*/React.createElement(CloseSquareOutlined, {
    style: {
      color: "red"
    },
    onClick: hideTextField
  })) : /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Insert Text"
  }, /*#__PURE__*/React.createElement("div", {
    onClick: showTextField
  }, /*#__PURE__*/React.createElement("h4", {
    className: "m-0",
    style: {
      cursor: "pointer"
    }
  }, "T"))), /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Set width"
  }, /*#__PURE__*/React.createElement("input", {
    className: "input-type",
    type: "range",
    min: "1",
    max: "30",
    style: {
      height: "5px"
    },
    value: lineWidth,
    onChange: e => {
      onChange(e, "lineWidth");
    }
  })), /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Choose Color"
  }, /*#__PURE__*/React.createElement("input", {
    className: "input-type color_inpt",
    type: "color",
    value: color,
    onChange: e => {
      onChange(e, "color");
    }
  })), /*#__PURE__*/React.createElement(Tippy, {
    placement: "down",
    content: "Delete Text"
  }, /*#__PURE__*/React.createElement(SyncOutlined, {
    style: {
      color: "primary"
    },
    onClick: () => {
      setModalState({ ...modalState,
        textList: textList.filter((elem, index) => index !== textList.length - 1)
      });
    }
  }))), /*#__PURE__*/React.createElement("div", {
    className: "butns"
  }, /*#__PURE__*/React.createElement(Button, {
    className: "submit_btn",
    onClick: addBugs,
    variant: "primary"
  }, "Submit"))))), /*#__PURE__*/React.createElement(Modal.Body, {
    ref: modalRef
  }, /*#__PURE__*/React.createElement("div", {
    className: "main m-0 scrollable_canvas"
  }, textList && textList.map((element, index) => {
    return /*#__PURE__*/React.createElement(React.Fragment, {
      key: index
    }, /*#__PURE__*/React.createElement("p", {
      draggable: true,
      onDragEnd: dragEnd,
      className: "text-box hand-pointer"
    }, element));
  }), isEditing && /*#__PURE__*/React.createElement("textarea", {
    value: text,
    onChange: saveText,
    onBlur: saveTextList,
    draggable: true,
    onDragEnd: dragEnd,
    className: "text-box boxes hand-pointer"
  }), /*#__PURE__*/React.createElement(TransformWrapper, {
    defaultScale: 1,
    defaultPositionX: 1,
    defaultPositionY: 100,
    panning: {
      disabled: dragZoom
    }
  }, ({
    zoomIn,
    zoomOut,
    resetTransform,
    ...rest
  }) => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ButtonGroup, {
    "aria-label": "Zoom",
    className: "btn_grp"
  }, /*#__PURE__*/React.createElement(Button, {
    variant: "secondary",
    className: "z_hand pt-0 dragBtn",
    onClick: () => setDrag()
  }, "Hand"), /*#__PURE__*/React.createElement(Button, {
    variant: "secondary",
    className: "z_in pt-0",
    onClick: () => zoomIn()
  }, " ", /*#__PURE__*/React.createElement(ZoomInOutlined, null)), /*#__PURE__*/React.createElement(Button, {
    variant: "secondary",
    className: "z_out pt-0",
    onClick: () => zoomOut()
  }, /*#__PURE__*/React.createElement(ZoomOutOutlined, null)), /*#__PURE__*/React.createElement(Button, {
    variant: "secondary",
    className: "z_re pt-0",
    onClick: () => resetTransform()
  }, /*#__PURE__*/React.createElement(RedoOutlined, null))), /*#__PURE__*/React.createElement(TransformComponent, null, /*#__PURE__*/React.createElement("canvas", {
    id: "draw",
    className: "canvas_ele",
    onMouseDown: startDrawing,
    onMouseUp: endDrawing,
    onMouseMove: draw,
    ref: canvasRef
  }, /*#__PURE__*/React.createElement("img", {
    src: image,
    alt: ""
  }))))))), /*#__PURE__*/React.createElement("div", {
    className: "footer"
  }, /*#__PURE__*/React.createElement("label", {
    htmlFor: "description-name",
    className: "col-form-label"
  }, "Description"), /*#__PURE__*/React.createElement("textarea", {
    type: "textarea",
    className: "form-control text-area",
    id: "description",
    onChange: e => {
      onChange(e, "description");
    },
    value: description
  })))));
}